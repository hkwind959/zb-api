
package rest.zb.rest.api;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSONObject;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.TimeInterval;
import entity.commons.UserApi;
import entity.enumtype.SymbolEnum;
import entity.enumtype.TradeEnum;
import jodd.util.StringUtil;
import kits.my.EncryDigestUtil;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import rest.zb.api.util.MapSort;
import rest.zb.rest.entity.Order;
import rest.zb.rest.entity.TradeResult;
import rest.zb.rest.entity.post.Account;

@Data
@Slf4j
public class RestApiPost {
	private String symbol;
	private UserApi userApi;
	private String url;

//	public static String url = "https://trade.zb.com/api/";

	public RestApiPost(String url, SymbolEnum symbol, UserApi userApi) {
		super();
		this.userApi = userApi;
		this.symbol = symbol.name();
		this.url = url;
	}

	public RestApiPost(String url, String symbol, UserApi userApi) {
		super();
		this.userApi = userApi;
		this.symbol = symbol;
		this.url = url;
	}

	/**
	 * 获取子账号列表接口
	 */
	public void getSubUserList() {
		TimeInterval timer = DateUtil.timer();
		Map<String, String> params = new LinkedHashMap<String, String>();
		// 保持队形1.method,2.accesskey
		params.put("method", "getSubUserList");
		params.put("accesskey", userApi.getApiKey());// 这个需要加入签名,放前面
		log.debug("参数:" + params);
		String json = this.getJsonPost(params);
		log.debug("执行时间:" + timer.intervalMs() + "子账户列表::" + json);
	}
	
	 /**
     * 添加子账号接口
     */
    public void addSubUser() {
    	TimeInterval timer = DateUtil.timer();
		Map<String, String> params = new HashMap<String, String>();
		params.put("accesskey", userApi.getApiKey());// 这个需要加入签名,放前面
		
		params.put("method", "addSubUser");
		params.put("password", "pass123457");
		params.put("subUserName", "test33");
		params.put("memo", "hah");
		
		
		
		log.debug("下单参数:" + params.toString());
		String json = getJsonPost(params);
		log.debug("执行时间:" + timer.intervalMs() + "添加子账号接口返回:" + json);
    }
	
	
	/**
	 * 创建子账号APIkey
	 */
	public void createSubUserKey() {
		TimeInterval timer = DateUtil.timer();
		Map<String, String> params = new LinkedHashMap<String, String>();
		params.put("method", "createSubUserKey");
		params.put("accesskey", userApi.getApiKey());// 这个需要加入签名,放前面

		params.put("toUserId", "1325607");
		params.put("keyName", "myKey11");
//		params.put("apiIpBind", "");//不要传""
		params.put("assetPerm", "true");//查询资产权限：查询账号及资产情况:0-不可以，1:可以'
		params.put("entrustPerm", "true");//委托交易权限：委托,取消,查询交易订单:0-不可以，1:可以
		params.put("moneyPerm", "true");//充值提币权限：获取充值,提币地址和记录，发起提币业务:0-不可以，1:可以
		params.put("leverPerm", "true");//杠杆理财权限：理财管理,借贷管理:0-不可以，1:可以
		
		
		log.debug("下单参数:" + params.toString());
		String json = getJsonPost(params);
		log.debug("执行时间:" + timer.intervalMs() + "下单返回:" + json);
	}

	/** 现价买入 */
	public TradeResult buy(double price, double amount) {

		TimeInterval timer = DateUtil.timer();
		Map<String, String> params = new LinkedHashMap<String, String>();
		params.put("method", "order");
		params.put("accesskey", userApi.getApiKey());// 这个需要加入签名,放前面

		params.put("price", Convert.toStr(price));
		params.put("amount", Convert.toStr(amount));
		params.put("tradeType", "1");// 交易类型1/0[buy/sell]
		params.put("currency", symbol);// 交易类型(目前仅支持BTC/LTC/ETH)
		log.debug("下单参数:" + params.toString());
		String json = getJsonPost(params);
		log.debug("执行时间:" + timer.intervalMs() + "下单返回:" + json);
		return JSONObject.parseObject(json, TradeResult.class);
	}

	/** 现价卖出 */
	public TradeResult sell(double price, double amount) {
		TimeInterval timer = DateUtil.timer();
		Map<String, String> params = new LinkedHashMap<String, String>();
		params.put("method", "order");
		params.put("accesskey", userApi.getApiKey());// 这个需要加入签名,放前面

		params.put("price", Convert.toStr(price));
		params.put("amount", Convert.toStr(amount));
		params.put("tradeType", "0");// 交易类型1/0[buy/sell]
		params.put("currency", symbol);// 交易类型(目前仅支持BTC/LTC/ETH)
		log.debug("下单参数:" + params.toString());
		String json = getJsonPost(params);
		log.debug("执行时间:" + timer.intervalMs() + "下单返回:" + json);
		return JSONObject.parseObject(json, TradeResult.class);
	}

	/** 获取订单信息 **/
	public Order getOrder(long orderId) {
		TimeInterval timer = DateUtil.timer();
		Map<String, String> params = new LinkedHashMap<String, String>();
		// 保持队形1.method,2.accesskey,3.id,4.currency
		params.put("method", "getOrder");
		params.put("accesskey", userApi.getApiKey());//
		// 这个需要加入签名,放前面
		params.put("id", orderId + "");
		params.put("currency", symbol);// 默认
		String json = this.getJsonPost(params);

		log.debug("执行时间:" + timer.intervalMs() + "查询订单:" + json);
		return JSONObject.parseObject(json, Order.class);

	}

	/**
	 * 获取所有订单,获取多个委托买单或卖单，每次请求返回10条记录(返回null则没有订单)
	 *
	 * @param             tradeType交易类型1/0[buy/sell]
	 * @param pageIndex页数
	 */
	public void getOrders(TradeEnum tradeType, int pageIndex) {
		TimeInterval timer = DateUtil.timer();
		// 会抛出json{"code":3001,"message":"挂单没有找到"}
		Map<String, String> params = new LinkedHashMap<String, String>();
		// 保持队形1.method,2.accesskey,3.id,4.currency
		params.put("method", "getOrders");
		params.put("accesskey", userApi.getApiKey());//
		// 这个需要加入签名,放前面
		params.put("tradeType", Convert.toStr(tradeType.getType()));
		params.put("currency", symbol);
		params.put("pageIndex", pageIndex + "");
		// log.debug(params.toString());
		String json = getJsonPost(params);
		log.debug("执行时间:" + timer.intervalMs() + "获取多个委托:" + json);
	}

	/**
	 * 获取未成交或部份成交的买单和卖单，每次请求返回pageSize<=100条记录
	 *
	 * @param pageIndex
	 * @return
	 */
	public List<Order> getUnfinishedOrdersIgnoreTradeType(int pageIndex) {
		// 会抛出json{"code":3001,"message":"挂单没有找到"}
		Map<String, String> params = new LinkedHashMap<String, String>();
		// 保持队形1.method,2.accesskey,3.id,4.currency
		params.put("method", "getUnfinishedOrdersIgnoreTradeType");
		params.put("accesskey", userApi.getApiKey());//
		// 这个需要加入签名,放前面
		params.put("currency", symbol);
		params.put("pageIndex", Convert.toStr(pageIndex));
		params.put("pageSize", "10");// 每页数量
		log.debug("参数:" + params);
		String json = getJsonPost(params);
		log.debug("未成交订单:" + json);
		try {
			return JSONObject.parseArray(json, Order.class);
		} catch (Exception e) {

		}
		return new ArrayList<Order>();
	}

	/** 取消订单 **/
	public TradeResult cancelOrder(long orderId) {
		TimeInterval timer = DateUtil.timer();
		Map<String, String> params = new LinkedHashMap<String, String>();
		// 保持队形1.method,2.accesskey,3.id,4.currency
		params.put("method", "cancelOrder");
		params.put("accesskey", userApi.getApiKey());// 这个需要加入签名,放前面
		params.put("id", orderId + "");
		params.put("currency", symbol);// 默认
		String json = getJsonPost(params);
		log.debug("执行时间:" + timer.intervalMs() + "取消:" + json);
		return JSONObject.parseObject(json, TradeResult.class);
	}

	/** 获取用户信息 **/
	public Account getAccount() {
		TimeInterval timer = DateUtil.timer();
		Map<String, String> params = new LinkedHashMap<String, String>();
		// 保持队形1.method,2.accesskey
		params.put("method", "getAccountInfo");
		params.put("accesskey", userApi.getApiKey());// 这个需要加入签名,放前面
		log.debug("参数:" + params);
		String json = this.getJsonPost(params);
		log.debug("执行时间:" + timer.intervalMs() + "用户信息:" + json);

		return JSONObject.parseObject(json, Account.class);

	}

	/** 获取用户充值地址 */
	public String getUserAddress() {
		Map<String, String> params = new LinkedHashMap<String, String>();
		// 保持队形1.method,2.accesskey
		params.put("method", "getUserAddress");
		params.put("accesskey", userApi.getApiKey());// 这个需要加入签名,放前面
		params.put("currency", symbol.substring(0, symbol.indexOf("_")));// 默认,需要去掉_cny后缀

		String json = this.getJsonPost(params);
		return json;
	}

	/** 获取用户认证的提现地址 */
	public String getWithdrawAddress() {
		Map<String, String> params = new LinkedHashMap<String, String>();
		// 保持队形1.method,2.accesskey
		params.put("method", "getWithdrawAddress");
		params.put("accesskey", userApi.getApiKey());// 这个需要加入签名,放前面
		params.put("currency", symbol.substring(0, symbol.indexOf("_")));

		String json = this.getJsonPost(params);
		return json;
	}

	// /**提现*/
	//// 提现矿工费
	//// 比特币最低是0.0003
	//// 莱特币最低是0.001
	//// 以太币最低是0.01
	//// ETC最低是0.01
	//// BTS最低是3
	//// EOS最低是1
	public String withdraw(double amount, double fees, String receiveAddr) {
		Map<String, String> params = new LinkedHashMap<String, String>();
		// 保持队形1.method,2.accesskey

		params.put("accesskey", userApi.getApiKey());// 这个需要加入签名,放前面
		params.put("amount", Convert.toStr(amount));// 提现金额
		params.put("currency", symbol.substring(0, symbol.indexOf("_")));
		params.put("fees", Convert.toStr(fees));
		params.put("itransfer", "0");// 是否同意bitbank系内部转账(0不同意，1同意，默认不同意)
		params.put("method", "withdraw");
		params.put("receiveAddr", receiveAddr);// 接收地址（必须是认证了的地址，bts的话，以"账户_备注"这样的格式）
		params.put("safePwd", userApi.getPayPass());

		String json = this.getJsonPost(params);
		return json;
	}

	//
	/** 获取数字资产提现记录 */
	public String getWithdrawRecord() {
		Map<String, String> params = new LinkedHashMap<String, String>();
		// 保持队形1.method,2.accesskey
		params.put("method", "getWithdrawRecord");
		params.put("accesskey", userApi.getApiKey());// 这个需要加入签名,放前面
		params.put("currency", symbol.substring(0, symbol.indexOf("_")));
		params.put("pageIndex", "1");
		params.put("pageSize", "10");
		String json = this.getJsonPost(params);
		return json;
	}

	/** 获取数字资产充值记录 */
	public String getChargeRecord() {
		Map<String, String> params = new LinkedHashMap<String, String>();
		// 保持队形1.method,2.accesskey
		params.put("method", "getChargeRecord");
		params.put("accesskey", userApi.getApiKey());// 这个需要加入签名,放前面
		params.put("currency", symbol.substring(0, symbol.indexOf("_")));
		params.put("pageIndex", "1");
		params.put("pageSize", "10");
		String json = this.getJsonPost(params);
		return json;
	}
	// /**获取人民币提现记录*/
	// public String getCnyWithdrawRecord(){
	// Map<String, String> params = new LinkedHashMap<String, String>();
	// // 保持队形1.method,2.accesskey
	// params.put("method", "getCnyWithdrawRecord");
	// params.put("accesskey", userApi.getApiKey());// 这个需要加入签名,放前面
	// params.put("pageIndex", "1");
	// params.put("pageSize", "10");
	// log.debug(params);
	// String json = this.getJsonPost(params);
	// return json;
	// }
	// /**获取人民币充值记录*/
	// public String getCnyChargeRecord(){
	// Map<String, String> params = new LinkedHashMap<String, String>();
	// // 保持队形1.method,2.accesskey
	// params.put("method", "getCnyChargeRecord");
	// params.put("accesskey", userApi.getApiKey());// 这个需要加入签名,放前面
	// params.put("pageIndex", "1");
	// params.put("pageSize", "10");
	// log.debug(params);
	// String json = this.getJsonPost(params);
	// return json;
	// }

	/**
	 * 获取json内容
	 * 
	 * @param params
	 * @return
	 */
	private String getJsonPost(Map<String, String> params) {
		params.put("accesskey", userApi.getApiKey());// 这个需要加入签名,放前面
		// String paramsForStr = getParamsForStr(params);// 将参数字符串化
		String digest = EncryDigestUtil.digest(userApi.getSecretKey());

		// log.debug("SecretKey digest加密后:"+digest);
		// log.debug("要md5加密的字符串:"+paramsForStr);
		// String sign = EncryDigestUtil.hmacSign(paramsForStr, digest);
		log.info("加密参数拼接:"+toStringMap(params));
		String sign = EncryDigestUtil.hmacSign(toStringMap(params), digest);

		// log.debug("得到的sign:"+sign);
		String method = params.get("method");

		// 加入验证
		params.put("sign", sign);
		params.put("reqTime", Convert.toStr(System.currentTimeMillis()));

//		System.out.println("aa:"+toStringMap(params));
		// log.debug(Convert.toString(System.currentTimeMillis()));
		// HttpRequest request = HttpRequest.get(url + method).query(params);
		// log.debug(request.url());
		// String json = request.send().bodyText();
//		String json = "";
//		try {
//			json = HttpUtilManager.getInstance().requestHttpPost(url, method, params);
//			log.info(symbol+"函数:"+method+",返回:"+json);
//		} catch (HttpException | IOException e) {
//			log.error("获取交易json异常", e);
//		}
		String finalUrl = url + method + "?" + this.buildMap(params);
		log.info(method+"请求:"+finalUrl);
		String json = jodd.http.HttpRequest.get(finalUrl).send().bodyText();
		return json;
	}

	public String buildMap(Map<String, String> map) {
		StringBuffer sb = new StringBuffer();
		if (map.size() > 0) {
			for (String key : map.keySet()) {
				sb.append(key + "=");
				if (StringUtils.isEmpty(map.get(key))) {
					sb.append("&");
				} else {
					String value = map.get(key);
					try {
						value = URLEncoder.encode(value, "UTF-8");
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
					sb.append(value + "&");
				}
			}
		}
		return sb.toString();
	}

	public static void main(String[] args) {
		String params = "method=order&accesskey=f5f08725-6a14-4f95-a9cb-25f2299dff2f&price=10&amount=0.01&tradeType=1&currency=etc_cny";
		String secretkey = "1be5ac67-94ce-4f8c-8c47-e73ed5242f25";
		String sign = EncryDigestUtil.hmacSign(params, EncryDigestUtil.digest(secretkey));
		log.debug(sign);
	}

	/**
	 * 将参数集转为字符串
	 * 
	 * @param params
	 * @return
	 */
	private String getParamsForStr(Map<String, String> params) {
		Iterator<String> it = params.keySet().iterator();
		String str = "";
		while (it.hasNext()) {
			String key = it.next();
			String value = params.get(key);
			str += key + "=" + value + "&";
		}
		str = StringUtil.substring(str, 0, str.length() - 1);
		return str;
	}

	public String toStringMap(Map m) {
		// 按map键首字母顺序进行排序
		m = MapSort.sortMapByKey(m);

		StringBuilder sbl = new StringBuilder();
		for (Iterator<Entry> i = m.entrySet().iterator(); i.hasNext();) {
			Entry e = i.next();
			Object o = e.getValue();
			String v = "";
			if (o == null) {
				v = "";
			} else if (o instanceof String[]) {
				String[] s = (String[]) o;
				if (s.length > 0) {
					v = s[0];
				}
			} else {
				v = o.toString();
			}
			if (!e.getKey().equals("sign") && !e.getKey().equals("reqTime") && !e.getKey().equals("tx")) {
				// try {
				// sbl.append("&").append(e.getKey()).append("=").append(URLEncoder.encode(v,
				// "utf-8"));
				// } catch (UnsupportedEncodingException e1) {
				// e1.printStackTrace();
				sbl.append("&").append(e.getKey()).append("=").append(v);
				// }
			}
		}
		String s = sbl.toString();
		if (s.length() > 0) {
			return s.substring(1);
		}
		return "";
	}
}
