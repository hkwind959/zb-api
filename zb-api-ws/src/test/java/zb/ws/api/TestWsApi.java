package zb.ws.api;

import java.io.IOException;

import entity.commons.UserApiEntity;
import entity.enumtype.ExchangeEnum;
import lombok.val;
import zb.ws.adapter.AdapterZb;

public class TestWsApi {

	public static void main(String[] args) throws IOException {
		
		val userApi = new UserApiEntity(ExchangeEnum.zb, "测试", "","");
//		val apiConfig = new ApiConfig("wss://api.zb.com:9999/websocket", "192.168.5.182", 23128);
		val api = new WsZb(userApi, new AdapterZb());
		String symbol= "eth_usdt";
//		api.depth(symbol);
		api.saveChannel("zbqc_depth");
//		api.saveChannel("ltcbtc_trades");
//		api.getAccount();
		
//		api.buy(symbol, 650, 0.001);
//		api.getOrder(symbol, 20180522105585216l, "测试");
//		api.cancel(symbol, 20180522105585216l, "取消");
	}
}
